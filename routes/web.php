<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['lock']], function () {
    Route::get('/', 'EventController@index')->name('events.index');
    Route::redirect('/events', '/');
    Route::get('/events/create', 'EventController@create')->name('events.create');
    Route::post('/events', 'EventController@store')->name('events.store');
    Route::post('/events/image', 'EventController@uploadImage');
    Route::get('/events/{event}', 'EventController@show')->name('events.show');
    Route::get('/events/{event}/edit', 'EventController@edit')->name('events.edit')->middleware('auth');;
    Route::post('/events/{event}/edit', 'EventController@update')->name('events.update')->middleware('auth');;

    Route::put('/events/{event}', 'EventController@publish')->name('events.publish')->middleware('auth');
    Route::delete('/events/{event}', 'EventController@destroy')->name('events.destroy')->middleware('auth');


    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login');
    Route::post('logout', 'LoginController@logout')->name('logout');

    Route::resource('admin', 'OptionController')->only([
        'index', 'edit', 'update',
    ])->middleware('auth');

    Route::get('about', 'OptionController@about')->name('about');
});

Route::get('unlock', 'LoginController@locked')->name('locked');
Route::post('unlock', 'LoginController@unlock')->name('unlock');
