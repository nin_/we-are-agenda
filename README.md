# We Are Agenda
Collaborative web calendar

## Contributing
Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on how (and what) to contribute.

## License
This project is licensed under the GNU Affero General Public License v3.0.
You can read [LICENSE](LICENSE) file for details.

### Credits
- The Laravel framework licensed under the MIT License.
- Tailwind CSS licensed under the MIT License.
