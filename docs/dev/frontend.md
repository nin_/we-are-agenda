# Frontend

We are agenda frontend is built with Laravel Mix, [PostCSS](https://github.com/postcss/postcss/tree/master/docs) and Tailwind CSS.

To start working on frontend you should run:
``` sh
npm run watch
```

## Style
The code is located in `/resources/sass/`

Constant values for the whole application such as colors, fonts and breakpoints
should be defined on `tailwind.config.js`.

[Tailwind Theme Configuration](https://tailwindcss.com/docs/theme/).

### Coding style

To avoid long class strings in the HTML use `@apply` with tailwind's classes.

To avoid performances issues with CSS cascading prefer first-level selectors.

Name your classes with the following pattern

~~~
    ComponentClass
    ComponentClass.modifierClass
    ComponentClass-descendantClass
~~~

Here's an example on how to apply theses 3 rules where `Footer` is the component's class, `list` & `link` descendants' classes and `active` a modifier.

``` html
<footer class="Footer">
    <nav class="Footer-list">
        <a class="Footer-link active" href="#">
            <svg class="Footer-icon">Foo</svg>
        </a>
        <a class="Footer-link" href="#">
            <svg class="Footer-icon">Bar</svg>
        </a>
        <a class="Footer-link" href="#">
            <svg class="Footer-icon">Baz</svg>
        </a>
    </nav>
</footer>
```

``` scss
.Footer {
  @apply fixed bottom-0 w-full shadow-2xl bg-white;

  &-list {
    @apply flex justify-around;
  }

  &-link {
    @apply inline-block py-2 px-4 text-gray-800;

    &:hover, &.active {
      @apply text-black border-b-4 border-black;
    }
  }
}
```

**Coding tip:**

If you want to use your IDE autocomplete for tailwind's classes:
- start by putting them in the HTML and when it's ready, cut-and-paste them in the scss.
- make sure that the compiled CSS isn't ignored by your IDE.
