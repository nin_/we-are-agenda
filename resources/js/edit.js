import Editor from '@toast-ui/editor';
import 'codemirror/lib/codemirror.css'; // Editor's Dependency Style
import '@toast-ui/editor/dist/toastui-editor.css'; // Editor's Style

const
    el = document.getElementById('editor'),
    textarea = document.getElementById(el.dataset.textarea);
textarea.classList.add('hidden');

const getCSRF = () => {
    const parts = `; ${document.cookie}`.split(`; XSRF-TOKEN=`);
    if (parts.length === 2) return decodeURIComponent(parts.pop().split(';').shift());
}

const uploadImage = async (blob) => {
    const formData = new FormData();
    formData.append("image", blob, "image.png");
    const response = await fetch('/events/image', {
        method: 'POST',
        headers: {
            'X-XSRF-TOKEN': getCSRF(),
        },
        body: formData
    });

    return await response.json();
}

const editor = new Editor({
    usageStatistics: false,
    el: el,
    initialValue: textarea.value,
    height: '500px',
    initialEditType: 'markdown',
    previewStyle: 'vertical',
    hideModeSwitch: true,
    toolbarItems: [
        'heading',
        'bold',
        'italic',
        'strike',
        'divider',
        'hr',
        'quote',
        'divider',
        'ul',
        'ol',
        'divider',
        'image',
        'table',
        'link',
    ],
    events: {
        change: () => {
            textarea.value = editor.getMarkdown();
        }
    },
    hooks: {
        addImageBlobHook: (blob, callback) => {
            const path = uploadImage(blob).then(path => callback(path));
            return false;
        }
    }
});
