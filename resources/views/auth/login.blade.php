@extends('layouts.app')

@section('title', __('user.login'))

@section('content')
    <form action="{{ route('login') }}" method="post">
        @csrf
        <div class="flex mb-3">
            <div class="w-full">
                <label class="block uppercase tracking-wide text-gray-700 font-bold mb-2" for="name">
                    @lang('user.username')
                </label>
                <input
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                    required id="name" name="name" type="text" autocomplete="username">
            </div>
        </div>
        <div class="flex mb-3">
            <div class="w-full">
                <label class="block uppercase tracking-wide text-gray-700 font-bold mb-2" for="password">
                    @lang('user.password')
                </label>
                <input
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                    required id="password" name="password" type="password" autocomplete="current-password">
            </div>
        </div>

        <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
        <label for="remember">
            @lang('user.remember')
        </label>

        <div class="flex mb-3">
            <input type="submit" class="btn blue" value="@lang('user.login')">
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li class="text-red-500 italic">{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </form>
@endsection
