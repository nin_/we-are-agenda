@extends('layouts.html')

@section('title', __('auth.unlock'))

@section('body')
    <main class="container mx-auto p-3">
        <div class="bg-red-100 border-t border-b border-red-500 text-red-900 px-4 py-3 mb-2" role="alert">
            {!! $banner !!}
        </div>

        <form action="{{ route('unlock') }}" method="post">
            @csrf

            <label class="block uppercase tracking-wide text-gray-700 font-bold mb-2" for="password">
                @lang('user.password')
            </label>
            <input
                class="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 mb-3"
                id="password" name="password" type="password" autocomplete="off">

            <div class="flex">
                <input type="submit" class="btn blue" value="@lang('auth.unlock')">
            </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li class="text-red-500 italic">{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </form>
    </main>
@endsection
