@extends('layouts.html')

@section('body')
    <header>
        <nav class="bg-orange-700 text-white px-4">
            <ul class="flex flex-wrap py-4">
                <li class="flex items-center flex-shrink-0 mr-6">
                    <a href="{{url('/')}}" class="font-semibold text-xl">
                        {{ \App\Http\Controllers\OptionController::getOption('site_name') }}
                    </a>
                </li>
                <li class="flex items-center flex-shrink-0 mr-6">
                    <a href="{{route('events.create')}}">@lang('nav.publish')</a>
                </li>
                <li class="flex items-center flex-shrink-0 mr-6">
                    <a href="{{route('about')}}">@lang('nav.about')</a>
                </li>
                @if(Auth::check())
                    <li class="flex items-center flex-shrink-0 mr-6">
                        <a href="{{route('admin.index')}}">@lang('nav.admin')</a>
                    </li>
                    <form action="{{route('logout')}}" method="POST" class="flex items-center flex-shrink-0 mr-6">
                        @csrf
                        <input
                            type="submit"
                            class="bg-transparent cursor-pointer"
                            value="@lang('nav.logout')"
                        >
                    </form>
                @endif
            </ul>
        </nav>
    </header>
    <main class="container mx-auto p-3">
        @yield('content')
    </main>
    @stack('scripts')
@endsection
