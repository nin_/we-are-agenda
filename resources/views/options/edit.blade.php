@extends('layouts.app')

@section('title', __('admin.edit').' '.$name)

@section('content')
    <form
        action="{{route('admin.update', ['admin' => $name])}}"
        method="post"
    >
        <h1><label class="font-semibold text-3xl" for="value">@lang('admin.edit') {{ $name }}</label></h1>
        <p>@lang('admin.optionDescription.'.$name)</p>

        @yield('inputs')

        <input type="submit" class="mt-2 block btn btn blue" value="@lang('admin.update')">
        @csrf
        @method('PATCH')
    </form>
@endsection

@push('scripts')
    <script src="{{ mix('js/edit.js') }}" type="module"></script>
@endpush
