@extends('layouts.app')

@section('title', __('nav.admin'))

@section('content')
    <h1 class="font-semibold text-center text-3xl mb-3">@lang('nav.admin')</h1>

    <table class="w-full">
        <thead>
        <tr>
            <th class="px-4 py-2">@lang('admin.option')</th>
            <th class="px-4 py-2">@lang('admin.description')</th>
            <th class="px-4 py-2">@lang('admin.value')</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($options as $name => $value)
            <tr>
                <td class="border px-4 py-2">
                    <a class="underline text-orange-700 orahover:text-black" href="{{ route('admin.edit', ['admin' => $name]) }}">
                        {{ $name }}
                    </a>
                </td>
                <td class="border px-4 py-2">@lang('admin.optionDescription.'.$name)</td>
                <td class="border px-4 py-2"><code>{{ $value }}</code></td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
