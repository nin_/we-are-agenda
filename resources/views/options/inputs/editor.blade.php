@extends('options.edit')

@section('inputs')
    <textarea
        class="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500  @error('description') border-red-500 @enderror"
        id="value" name="value">{{ $value }}</textarea>
    <div id="editor" data-textarea="value"></div>
    <noscript class="text-gray-600 italic">
        <p class="text-gray-600 italic">@lang('form.use_markdown')</p>
    </noscript>
@endsection

@push('scripts')
    <script src="{{ mix('js/edit.js') }}" type="module"></script>
@endpush
