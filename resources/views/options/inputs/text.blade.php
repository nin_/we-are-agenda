@extends('options.edit')

@section('inputs')
    <input
        class="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
        type="text"
        id="value"
        name="value"
        value="{{ $value }}"
    >
@endsection

@push('scripts')
    <script src="{{ mix('js/edit.js') }}" type="module"></script>
@endpush
