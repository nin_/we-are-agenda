@extends('options.edit')

@section('inputs')
    <input type="checkbox" id="value" name="value" @if($value) checked @endif>
@endsection

@push('scripts')
    <script src="{{ mix('js/edit.js') }}" type="module"></script>
@endpush
