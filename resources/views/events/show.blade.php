@extends('layouts.app')

@section('title', \Carbon\Carbon::parse($event->date)->isoFormat('LL').' '.$event->title)

@section('content')
    @if(isset($event->draft) && $event->draft == true)
        <div class="bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md" role="alert">
            <p class="font-bold">@lang('event.awaiting_banner.title')</p>
            @if(\Illuminate\Support\Facades\Auth::guest())<p class="text-sm">@lang('event.awaiting_banner.body')</p>@endif
        </div>
    @endif

    @if(Auth::check())
        {{-- Moderation controls --}}
        <div class="py-4">
            <h2 class="font-semibold text-xl">Moderation</h2>
            @if($event->contact)<p>@lang('event.contact') {{ $event->contact }}</p>@endif
            <div class="flex">
                <a
                    class="btn blue"
                    href="{{route('events.edit', $event->slug)}}"
                >
                    @lang('admin.edit')
                </a>
                <form action="{{route('events.destroy', $event->slug)}}" method="POST">
                    @method('DELETE')
                    @csrf
                    <input
                        type="submit"
                        class="cursor-pointer bg-red-300 hover:bg-red-400 font-bold py-2 px-4 @if($event->draft) rounded-l @else rounded @endif"
                        value="@lang('event.delete')"
                    >
                </form>
                @if($event->draft)
                    <form action="{{route('events.publish', $event->slug)}}" method="POST">
                        @method('PUT')
                        @csrf
                        <input
                            type="submit"
                            class="cursor-pointer bg-green-300 hover:bg-green-400 font-bold py-2 px-4 rounded-r"
                            value="@lang('event.publish')"
                        >
                    </form>
                @endif
            </div>
        </div>
    @endif

    <div class="flex flex-wrap">
        <article class="w-full">
            <h1 class="font-semibold text-3xl">{{$event->title}}</h1>
            <div class="text-xl flex justify-between flex-wrap">
                <time class="py-1">
                    {{ \Carbon\Carbon::parse($event->date)->isoFormat('LL') }}
                    @lang('event.location_at') {{ \Carbon\Carbon::parse($event->time_start)->format('H:i') }}
                    @if($event->time_end)
                    @lang('event.location_to') {{ \Carbon\Carbon::parse($event->time_end)->format('H:i') }}
                    @endif
                </time>
                @isset($event->location)
                    <p class="py-1">{{ $event->location }}</p>
                @endisset
            </div>

            <div class="pt-4 prose max-w-none">
                {!! $event->description !!}
            </div>
        </article>
    </div>

@endsection
