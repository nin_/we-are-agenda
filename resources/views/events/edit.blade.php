@extends('layouts.app')

@section('title', $edit ? __('event.edit') : __('nav.publish'))

@section('content')
    @if(!$edit && !\App\Http\Controllers\OptionController::getOption('no_review'))
        <div class="bg-teal-100 border-t border-b border-teal-500 text-teal-900 px-4 py-3 mb-2" role="alert">
            <p class="font-bold">@lang('event.creation_banner.title')</p>
            <p class="text-sm">@lang('event.creation_banner.body', ['about' => route('about')])</p>
        </div>

        <h1 class="font-semibold text-3xl">@lang('event.submitTitle')</h1>
    @else
        <h1 class="font-semibold text-3xl">@lang('event.edit')</h1>
    @endif

    <form action="{{ $edit ? route('events.update', ['event' => $event->slug]) : route('events.store')}}" method="post">
        @csrf
        <div class="flex mb-3">
            <div class="w-full">
                <label class="block uppercase tracking-wide text-gray-700 font-bold mb-2" for="title">
                    @lang('event.title')
                </label>
                <input
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 @error('title') border-red-500 @enderror"
                    id="title" name="title" type="text" @isset($event->title)value="{{$event->title}}"@endisset
                >
                <p class="text-gray-600 italic">@lang('form.required')</p>
                @error('title')
                <p class="text-red-500 italic">{{ $message }}</p>
                @enderror
            </div>
        </div>
        <div class="flex flex-wrap mb-3">
            <div class="w-full md:w-1/2">
                <label class="block uppercase tracking-wide text-gray-700 font-bold mb-2" for="date">
                    @lang('event.date')
                </label>
                <input
                    id="date"
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white @error('date') border-red-500 @enderror"
                    required
                    type="date"
                    pattern="\d{4}-\d{2}-\d{2}"
                    name="date"
                    min="{{ date('Y-m-d') }}"
                    max="{{ date('Y-m-d', strtotime('+6 months')) }}"
                    @isset($event->date)value="{{$event->date}}"@endisset
                >
                <p class="text-gray-600 italic">@lang('form.required')</p>
                @error('date')
                <p class="text-red-500 italic">{{ $message }}</p>
                @enderror
            </div>
            <div class="w-full md:w-1/4 md:px-3">
                <label class="block uppercase tracking-wide text-gray-700 font-bold mb-2" for="time_start">
                    @lang('event.time_start')
                </label>
                <input
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500  @error('time_start') border-red-500 @enderror"
                    required id="time_start" name="time_start" type="time" pattern="[0-9]{2}:[0-9]{2}"
                    @isset($event->time_start)value="{{ substr($event->time_start, 0, 5) }}"@endisset
                >
                <p class="text-gray-600 italic">@lang('form.required')</p>
                @error('time_start')
                <p class="text-red-500 italic">{{ $message }}</p>
                @enderror
            </div>
            <div class="w-full md:w-1/4 md:pl-3">
                <label class="block uppercase tracking-wide text-gray-700 font-bold mb-2" for="time_end">
                    @lang('event.time_end')
                </label>
                <input
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500  @error('time_end') border-red-500 @enderror"
                    id="time_end" name="time_end" type="time" pattern="[0-9]{2}:[0-9]{2}"
                    @isset($event->time_end)value="{{ substr($event->time_end, 0, 5) }}"@endisset
                >
                <p class="text-gray-600 italic">@lang('form.optional')</p>
                @error('time_end')
                <p class="text-red-500 italic">{{ $message }}</p>
                @enderror
            </div>
        </div>
        <div class="flex mb-3">
            <div class="w-full">
                <label class="block uppercase tracking-wide text-gray-700 font-bold mb-2" for="location">
                    @lang('event.location')
                </label>
                <input
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500  @error('location') border-red-500 @enderror"
                    id="location" name="location" type="text"
                    @isset($event->location)value="{{$event->location}}"@endisset
                >
                <p class="text-gray-600 italic">@lang('form.optional')</p>
                @error('location')
                <p class="text-red-500 italic">{{ $message }}</p>
                @enderror
            </div>
        </div>
        <div class="flex mb-3">
            <div class="w-full">
                <label class="block uppercase tracking-wide text-gray-700 font-bold mb-2" for="description">
                    @lang('event.description')
                </label>
                <textarea
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500  @error('description') border-red-500 @enderror"
                    required id="description"
                    name="description">@isset($event->description){{$event->description}}@endisset</textarea>
                <div id="editor" data-textarea="description"></div>
                <p class="text-gray-600 italic">@lang('form.required')</p>
                <noscript class="text-gray-600 italic">
                    <p class="text-gray-600 italic">@lang('form.use_markdown')</p>
                </noscript>
                @error('description')
                <p class="text-red-500 italic">{{ $message }}</p>
                @enderror
            </div>
        </div>
        <div class="flex mb-3">
            <div class="w-full">
                <label class="block uppercase tracking-wide text-gray-700 font-bold mb-2" for="contact">
                    @lang('event.email')
                </label>
                <input
                    class="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500  @error('contact') border-red-500 @enderror"
                    id="contact" name="contact" type="email" @isset($event->contact)value="{{$event->contact}}"@endisset
                >
                <p class="text-gray-600 italic">
                    @lang('form.optional')
                </p>
                <p class="text-gray-600 italic">
                    @lang('event.email_why')
                </p>
                @error('contact')
                <p class="text-red-500 italic">{{ $message }}</p>
                @enderror
            </div>
        </div>

        {{-- bot detection --}}
        @if(!$edit)
            <input type="text" name="name" class="hidden" aria-hidden="true">
            <input type="text" name="time" class="hidden" aria-hidden="true" value="{{time()}}">
        @endif

        <input type="submit" class="btn blue" value="@lang('event.submit')">
    </form>
@endsection

@push('scripts')
    <script src="{{ mix('js/edit.js') }}" type="module"></script>
@endpush
