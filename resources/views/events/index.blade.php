@extends('layouts.app')

@section('title', __('nav.incoming'))

@section('content')
    @if(!empty($banner))
        <div class="bg-teal-100 border-t border-b border-teal-500 text-teal-900 px-4 py-3 mb-2" role="alert">
            {!! $banner !!}
        </div>
    @endif


    @if(Auth::check()) {{-- Moderation controls --}}
    <aside class="w-full mb-3">
        <ul class="flex">
            <li class="flex-1 mr-2">
                <a
                    @if('1' === app('request')->input('drafted'))
                    class="text-center block py-2 px-4 btn teal"
                    href="{{url('')}}"
                    @else
                    class="text-center block py-2 px-4 btn teal active"
                    @endif
                >
                    @lang('event.published')
                </a>
            </li>
            <li class="flex-1 mr-2">
                <a
                    @if('1' === app('request')->input('drafted'))
                    class="text-center block py-2 px-4 btn teal active"
                    @elseif (0 === $draftedNumber)
                    class="text-center block py-2 px-4 btn teal disabled"
                    @else
                    class="text-center block py-2 px-4 btn teal"
                    href="{{url('?drafted=1')}}"
                    @endif
                >
                    {{ $draftedNumber }} @lang('event.awaiting')
                </a>
            </li>
        </ul>
    </aside>
    @endif

    @foreach ($calendar as $month => $days)
        <h2 class="font-semibold text-center text-3xl mb-3">{{ $month }}</h2>

        @foreach($days as $day => $events)
            <section class="w-full rounded shadow-lg mb-10">
                <h3 class="flex bg-orange-800 text-white px-4">
                    <span class="w-1/3">
                        @if (\Carbon\Carbon::parse($day)->isToday())
                            @lang('event.today')
                        @elseif (\Carbon\Carbon::parse($day)->isTomorrow())
                            @lang('event.tomorrow')
                        @endif
                    </span>
                    <span class="font-semibold w-1/3 text-center">{{ ucfirst(\Carbon\Carbon::parse($day)->translatedFormat('l j')) }}</span>
                    <span class="w-1/3 text-right">{{ ucfirst(\Carbon\Carbon::parse($day)->translatedFormat('F')) }}</span>
                </h3>

                @foreach ($events as $event)
                    <div class="border-solid border-b border-gray-600">
                        <time class="w-12 inline-block">
                            {{ \Carbon\Carbon::parse($event->time_start)->format('H:i') }}
                        </time>
                        <a class="hover:bg-gray-300" href="{{url('/events/'.$event->slug)}}">{{ $event->title }}</a>
                    </div>
                @endforeach
            </section>
        @endforeach
    @endforeach
@endsection
