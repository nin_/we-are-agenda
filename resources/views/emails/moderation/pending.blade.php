@lang('event.awaiting_banner.title') {{-- --}}

{{ route('events.show', ['event' => $event->slug]) }}

@lang('mail.pendingModeration.title') {{ $event->title }}

@if($event->contact)
@lang('mail.pendingModeration.contact') {{ $event->contact }}

@endif
@lang('mail.pendingModeration.date') {{ $event->date }}, {{ $event->time_start }}@if($event->time_end) to {{ $event->time_end }}@endif {{-- --}}

@lang('mail.pendingModeration.description')  {{-- --}}
{{ $event->description }}
