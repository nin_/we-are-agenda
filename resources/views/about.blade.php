@extends('layouts.app')

@section('title', __('nav.about'))

@section('content')
    <h1 class="font-semibold text-3xl">@lang('nav.about')</h1>

    <section class="prose">
        {!! $about !!}
    </section>

    <section class="prose">
        <h2 id="publication-criteria">@lang('about.publicationCriteria')</h2>

        {!! $publicationCriteria !!}
    </section>

    <section class="prose">
        <h2 id="publication-criteria">@lang('about.we_are_agenda.title')</h2>

        @lang('about.we_are_agenda.body')
    </section>


@endsection
