<?php

return [
    'username' => 'Username',
    'password' => 'Password',
    'login' => 'Login',
    'remember' => 'Remember Me',
];
