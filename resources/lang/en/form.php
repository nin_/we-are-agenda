<?php

return [
    'required' => 'Required',
    'optional' => 'Optional',
    'use_markdown' => 'Use markdown syntax to format it.',
];
