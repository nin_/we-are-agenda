<?php

return [
    'option' => 'Option',
    'description' => 'Description',
    'value' => 'Value',
    'optionDescription' => [
        'site_name' => 'The title of the site.',
        'no_review' => 'Allow users to publish event without moderation validation.',
        'publication_criteria' => 'Criteria for the moderation to publish an event. It is displayed on the about page.',
        'about_content' => 'Content of the about page.',
        'home_banner' => 'Display a message on the front page with a banner. When empty there is no banner.',
        'locked' => 'Lock the site behind a password.',
        'locked_password' => 'The password protecting the locked site.',
        'locked_banner' => 'The message displayed when the password is asked on a locked site.',
    ],
    'edit' => 'Edit',
    'update' => 'Update',
    'bool' => [
        'false' => 'No',
        'true' => 'Yes',
    ]
];
