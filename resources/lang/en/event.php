<?php

return [
    'creation_banner' => [
        'title' => 'Publication is not systematic.',
        'body' => 'It will be reviewed by moderation to enforce <a href=":about#publication-criteria" class="underline hover:text-black">publication criteria</a>.'
    ],
    'submitTitle' => 'Submit an event',
    'edit' => 'Edit the event',
    'title' => 'Title',
    'date' => 'Date',
    'time_start' => 'Start time',
    'time_end' => 'End time',
    'location' => 'Location',
    'description' => 'Description',
    'email' => 'Email of contact',
    'email_why' => 'This is used to contact you if needed and is only displayed to moderation.',
    'submit' => 'Submit event',

    'published' => 'Published',
    'awaiting' => 'awaiting moderation',
    'today' => 'Today',
    'tomorrow' => 'Tomorrow',

    'awaiting_banner' => [
        'title' => 'This event awaits moderation',
        'body' => 'This event isn\'t published yet. It will be reviewed by the moderation.'
    ],
    'location_at' => 'at',
    'delete' => 'Delete',
    'publish' => 'Publish',
    'contact' => 'Contact:',
];
