<?php

return [
    'pendingModeration' => [
        'subject' => 'A new event has been submitted',
        'title' => 'Title:',
        'contact' => 'Contact:',
        'date' => 'Date:',
        'description' => 'Description:',
    ]
];
