<?php

return [
    'publish' => 'Publish an event',
    'about' => 'About',
    'admin' => 'Admin',
    'logout' => 'Logout',
    'incoming' => 'Incoming events',
];
