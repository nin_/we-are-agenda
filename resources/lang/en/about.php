<?php

return [
    'publicationCriteria' => 'Publication criteria',
    'we_are_agenda' => [
        'title' => 'Powered by We Are Agenda',
        'body' => 'This site use the free software We Are Agenda.<br>
        It\'s distributed under the terms of the GNU Affero General Public License.<br>
        Source code and details available here: <a href="https://gitlab.com/nin_/we-are-agenda">https://gitlab.com/nin_/we-are-agenda</a>'
    ]
];
