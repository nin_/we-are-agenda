<?php

return [
    'pendingModeration' => [
        'subject' => 'Un nouvel évenement à été soumis',
        'title' => 'Titre :',
        'contact' => 'Contact :',
        'date' => 'Date :',
        'description' => 'Description :',
    ]
];
