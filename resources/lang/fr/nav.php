<?php

return [
    'publish' => 'Publier un événement',
    'about' => 'À propos',
    'admin' => 'Administration',
    'logout' => 'Déconnexion',
    'incoming' => 'Événements à venir',
];
