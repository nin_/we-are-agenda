<?php

return [
    'option' => 'Option',
    'description' => 'Description',
    'value' => 'Value',
    'optionDescription' => [
        'site_name' => 'Le nom du site.',
        'no_review' => 'Permettre aux utilisateurs de publier un événement sans validation de la modération.',
        'publication_criteria' => 'Les critères de publication de la modération pour publier un évenement. Ils sont affichés sur la page "À propos."',
        'about_content' => 'Le contenu de la page "À propos."',
        'home_banner' => 'Permet d\'afficher un message sur la page d\'accueil avec une bannière. Si rien nest spécifié il n\'y a pas de bannière.',
        'locked' => 'Verrouille le site derrière un mot de passe.',
        'locked_password' => 'Le mot de passe protegant le site si il est verrouillé.',
        'locked_banner' => 'Le message qui est affiché sur la page demandant le mot de passe sur un site verrouillé.',
    ],
    'edit' => 'Modifier',
    'update' => 'Update',
    'bool' => [
        'false' => 'Non',
        'true' => 'Oui',
    ]
];
