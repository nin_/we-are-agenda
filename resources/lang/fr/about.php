<?php

return [
    'publicationCriteria' => 'Critères de publication',
    'we_are_agenda' => [
        'title' => 'Propulsé par We Are Agenda',
        'body' => 'Ce site utilise le logiciel libre We Are Agenda.<br>
        Il est distribué sous les termes de la GNU Affero General Public License.<br>
        Le code source ainsi que plus de détails sont disponibles ici: <a href="https://gitlab.com/nin_/we-are-agenda">https://gitlab.com/nin_/we-are-agenda</a>'
    ]
];
