<?php

return [
    'required' => 'Requis',
    'optional' => 'Optionnel',
    'use_markdown' => 'Formattez avec la syntaxe Markdown.',
];
