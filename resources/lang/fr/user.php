<?php

return [
    'username' => 'Pseudo',
    'password' => 'Mot de passe',
    'login' => 'Se connecter',
    'remember' => 'Se souvenir de moi',
];
