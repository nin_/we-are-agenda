<?php

return [
    'creation_banner' => [
        'title' => 'La publication n\'est pas systématique.',
        'body' => 'Elle est conditionnée à la validatin de la modération pour faire respecter <a href=":about#publication-criteria" class="underline hover:text-black">les critères de publication</a>.'
    ],
    'submitTitle' => 'Soumettre un événement',
    'edit' => "Modifier l'événement",
    'title' => 'Titre',
    'date' => 'Date',
    'time_start' => 'Heure de début',
    'time_end' => 'Heure de fin',
    'location' => 'Lieu',
    'description' => 'Description',
    'email' => 'Courriel de contact',
    'email_why' => 'Il est utilisé pour vous contacter en cas de besoin et n\'est affiché qu\'à la modération.',
    'submit' => 'Soumettre un événement',

    'published' => 'Publié',
    'awaiting' => 'en attente de modération',
    'today' => 'Aujourd\'hui',
    'tomorrow' => 'Demain',

    'awaiting_banner' => [
        'title' => 'Cet événement attend d\'être modéré',
        'body' => 'Cet événement n\'est pas encore publié. Il sera examiné par la modération.'
    ],
    'location_at' => 'à',
    'delete' => 'Supprimer',
    'publish' => 'Publier',
    'contact' => 'Contact :',
];
