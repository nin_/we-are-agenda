<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class ListAdmin extends Command
{
    protected $signature = 'admin:list';

    protected $description = 'List the admins user';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $users = User::all(['name', 'email'])->toArray();

        $this->table(['name', 'email'], $users);
    }
}
