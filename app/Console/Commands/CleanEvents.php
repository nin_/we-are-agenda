<?php

namespace App\Console\Commands;

use App\Event;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class CleanEvents extends Command
{
    protected $signature = 'events:clean';

    protected $description = 'Delete old events';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $events = Event::query()->where('date', '<', Carbon::yesterday());

        foreach ($events->get('slug') as $event) {
            Event::clean($event->slug);
        }

        $events->delete();
    }
}
