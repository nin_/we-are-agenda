<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class DeleteAdmin extends Command
{
    protected $signature = 'admin:delete {name}';

    protected $description = 'Delete an admin account';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        User::where('name', $this->argument('name'))
            ->firstOrFail()
            ->delete();
    }
}
