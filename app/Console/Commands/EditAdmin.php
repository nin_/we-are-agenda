<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class EditAdmin extends Command
{
    protected $signature = 'admin:edit {name}';

    protected $description = 'Edit an admin user';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $user = User::where('name', $this->argument('name'))->firstOrFail();

        if ($this->confirm('Change name?')) {
            $user->name = $this->ask('New name:');
        }

        if ($this->confirm('Change password?')) {
            $user->password = Hash::make($this->secret('New password:'));
        }

        if (! $user->email) {
            if ($this->confirm('Add email?')) {
                $user->email = $this->ask('Email:');
            }
            $user->save();

            return;
        }

        if ($this->confirm('Delete email?')) {
            $user->email = null;
            $user->save();

            return;
        }

        if ($this->confirm('Edit email?')) {
            $user->email = $this->ask('New email:');
        }

        $user->save();
    }
}
