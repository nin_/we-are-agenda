<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class EditPage extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'max:60', 'min:3'],
            'slug' => [
                'required',
                'alpha_dash',
                'max:60',
                'min:3',
                'unique:pages,slug,'.$this->page.',slug',
                Rule::notIn(['events', 'pages', 'login', 'logout']),
            ],
            'content' => ['required', 'max:13120', 'min:60'],
        ];
    }
}
