<?php

namespace App\Http\Requests;

use App\Event;
use Illuminate\Foundation\Http\FormRequest;

class SubmitEvent extends FormRequest
{
    public function rules()
    {
        return array_merge(
            Event::RULES,
            [
                'name' => ['bail', 'present', 'same:""'], // honeypot
                'time' => ['bail', 'required', 'numeric', 'lt:'.(time() - 2)], // bot detection
            ]
        );
    }
}
