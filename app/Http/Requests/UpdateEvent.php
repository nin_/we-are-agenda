<?php

namespace App\Http\Requests;

use App\Event;
use Illuminate\Foundation\Http\FormRequest;

class UpdateEvent extends FormRequest
{
    public function rules()
    {
        return Event::RULES;
    }
}
