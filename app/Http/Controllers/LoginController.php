<?php

namespace App\Http\Controllers;

use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    public function username()
    {
        return 'name';
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout', 'locked', 'unlock');
    }

    public static function isUnlocked(Request $request): bool
    {
        if ($request->session()->has('unlocked')) {
            return true;
        }

        if (! OptionController::getOption('locked')) {
            $request->session()->put('unlocked', true);

            return true;
        }

        return false;
    }

    public function locked(Request $request)
    {
        if (self::isUnlocked($request)) {
            return redirect()->route('events.index');
        }

        return view('auth.unlock', ['banner' => OptionController::getBanner('locked')]);
    }

    public function unlock(Request $request)
    {
        $password = OptionController::getOption('locked_password');

        if ($password === $request->get('password')) {
            $request->session()->put('unlocked', true);
        }

        return redirect()->route('locked');
    }
}
