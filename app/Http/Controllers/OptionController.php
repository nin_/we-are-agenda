<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Parsedown;

class OptionController extends Controller
{
    private const OPTIONS = [
        'site_name' => [
            'type' => 'text',
            'default' => 'We Are Agenda',
        ],
        'no_review' => [
            'type' => 'boolean',
            'default' => false,
        ],
        'publication_criteria' => [
            'type' => 'editor',
        ],
        'about_content' => [
            'type' => 'editor',
        ],
        'home_banner' => [
            'type' => 'editor',
            'default' => false,
        ],
        'locked' => [
            'type' => 'boolean',
            'default' => false,
        ],
        'locked_password' => [
            'type' => 'text',
        ],
        'locked_banner' => [
            'type' => 'editor',
        ],
    ];

    public static function getOption($name)
    {
        if (! in_array($name, array_keys(self::OPTIONS))) {
            abort(404);
        }

        $cacheKey = 'option_'.$name;
        $option = Cache::get($cacheKey);

        if (null !== $option) {
            return $option;
        }

        $option = DB::selectOne(
            'select value from options where name = ?',
            [$name]
        );

        if (null === $option) {
            if (! isset(self::OPTIONS[$name]['default'])) {
                return null;
            }

            Cache::put($cacheKey, self::OPTIONS[$name]['default']);

            return self::OPTIONS[$name]['default'];
        }

        Cache::put($cacheKey, $option->value);

        return $option->value;
    }

    public function index()
    {
        $options = [];
        foreach (self::OPTIONS as $name => $option) {

            $options[$name]  = $option['type'] === 'boolean'
                ? __('admin.bool.'.var_export(self::getOption($name), true))
                : self::getOption($name);
        }

        return view('options.index', ['options' => $options]);
    }

    private function checkIfExist(string $name)
    {
        if (! in_array($name, array_keys(self::OPTIONS))) {
            abort(404);
        }
    }

    public function edit($name)
    {
        $this->checkIfExist($name);
        $type = self::OPTIONS[$name]['type'];

        if (! view()->exists('options.inputs.'.$type)) {
            abort(500);
        }

        return view('options.inputs.'.$type, ['name' => $name, 'value' => self::getOption($name)]);
    }

    public function update(Request $request, $name)
    {
        $this->checkIfExist($name);

        $type = self::OPTIONS[$name]['type'];
        $value = $request->get('value');

        if ('boolean' === $type) {
            $value = 'on' === $value;
        }

        if (null === DB::selectOne('select name from options where name = ?', [$name])) {
            DB::insert('insert into options (name, value) values (?, ?)', [$name, $value]);
        } else {
            DB::update('update options set value = ? where name = ?', [$value, $name]);
        }
        Cache::put('option_'.$name, $value);

        return redirect()->route('admin.edit', ['admin' => $name]);
    }

    public static function getBanner($name)
    {
        $parsedown = new Parsedown();

        return $parsedown->setSafeMode(true)->text(self::getOption($name.'_banner'));
    }

    public function about(Parsedown $parsedown)
    {
        return view('about', [
            'publicationCriteria' => $parsedown->setSafeMode(true)->text(self::getOption('publication_criteria')),
            'about' => $parsedown->setSafeMode(true)->text(self::getOption('about_content')),
        ]);
    }
}
