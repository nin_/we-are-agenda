<?php

namespace App\Http\Controllers;

use App\Event;
use App\Http\Requests\SubmitEvent;
use App\Http\Requests\UpdateEvent;
use App\Http\Requests\UploadImage;
use App\Mail\PendingModeration;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Parsedown;

class EventController extends Controller
{
    /**
     * Get the list of events
     * TODO: 6 months pages.
     */
    private function getList(bool $drafted = false)
    {
        return Event::query()
            ->select(['title', 'slug', 'date', 'time_start'])
            ->where('draft', '=', $drafted)
            ->where('date', '>=', Carbon::today())
            ->where('date', '<', Carbon::now()->add(6, 'months'))
            ->orderBy('date')
            ->orderBy('time_start')
            ->get();
    }

    public function index(Request $request)
    {
        if (Auth::check()) {
            return $this->authIndex($request);
        }

        return view('events.index', [
            'calendar' => $this->getHomeCalendar(),
            'banner' => OptionController::getBanner('home'),
        ]);
    }

    private function authIndex(Request $request)
    {
        $draftedNumber = Event::select('draft')
            ->where('draft', '=', true)
            ->where('date', '>=', Carbon::today())
            ->where('date', '<', Carbon::now()->add(6, 'months'))
            ->count('date');

        $calendar = '1' === $request->get('drafted')
                ? $this->collectionToCalendar($this->getList(true))
            : $this->getHomeCalendar();

        return view('events.index', [
            'calendar' => $calendar,
            'draftedNumber' => $draftedNumber ?? 0,
            'banner' => OptionController::getBanner('home'),
        ]);
    }

    private function getHomeCalendar(): array
    {
        $calendar = Cache::get('home_calendar');
        if (null !== $calendar) {
            return $calendar;
        }

        $calendar = $this->collectionToCalendar($this->getList());
        Cache::put('home_calendar', $calendar, now()->diffInSeconds(Carbon::tomorrow()));

        return $calendar;
    }

    private function collectionToCalendar(Collection $events): array
    {
        $calendar = [];
        foreach ($events as $event) {
            $calendar[ucfirst(Carbon::parse($event->date)->translatedFormat('F'))][$event->date][] = $event;
        }

        return $calendar;
    }

    public function create(Request $request)
    {
        if (null === $request->session()->get('tmp_asset_dir')) {
            $request->session()->put('tmp_asset_dir', uniqid());
        }

        return view(
            'events.edit',
            [
                'event' => (object) $request->old(),
                'edit' => false,
            ]);
    }

    public function edit(string $slug, Request $request)
    {
        return view(
            'events.edit',
            [
                'event' => (object) array_merge(Event::retrieveEvent($slug)->toArray(), $request->old()),
                'edit' => true,
            ]
        );
    }

    public function store(SubmitEvent $request)
    {
        $validated = $request->validated();
        $description = $validated['description'];
        unset($validated['description']);

        $event = Event::create($validated);
        $event->draft = !OptionController::getOption('no_review');

        $tmpDir = $request->session()->pull('tmp_asset_dir');
        $event->description = str_replace(
            'tmp_'.$tmpDir,
            $event->slug,
            $description
        );

        $event->save();
        if (Storage::exists('public/events/tmp_'.$tmpDir)) {
            Storage::move('public/events/tmp_'.$tmpDir, 'public/events/'.$event->slug);
        }

        // TODO: replace by url with a secret as query param
        $request->session()->put('submitted', $event->id);

        if ($event->draft) {
            Mail::bcc(User::whereNotNull('email')->get())->send(new PendingModeration($event));
        }

        return redirect()->route('events.show', ['event' => $event->slug]);
    }

    public function update(string $slug, UpdateEvent $request)
    {
        $validated = $request->validated();

        // Remove honey pot
        unset($validated['name'], $validated['time']);

        /** @var \Illuminate\Database\Eloquent\Builder $event */
        $event = Event::where('slug', $slug);
        $event->update($validated);
        Cache::forget('event_'.$slug);

        if ($this->isOnHomepage($event->firstOrFail())) {
            Cache::forget('home_calendar');
        }

        return redirect()->route('events.show', ['event' => $slug]);
    }

    public function show(string $slug, Request $request, Parsedown $parsedown)
    {
        $event = Event::retrieveEvent($slug);

        if ($event->draft && Auth::guest() && $request->session()->get('submitted') !== $event->id) {
            abort(404);
        }

        $event->description = $parsedown->setSafeMode(true)->text($event->description);

        return view('events.show', ['event' => $event]);
    }

    public function publish(string $slug)
    {
        $event = Event::findSlug($slug, ['id', 'draft']);
        $event->draft = false;
        $event->save();

        Cache::forget('home_calendar');

        return redirect()->route('events.index', ['drafted' => '1']);
    }

    public function destroy(string $slug)
    {
        $event = Event::findSlug($slug, ['id', 'slug', 'draft']);
        $cleanCache = ! $event->draft;

        $event->delete();

        if ($cleanCache) {
            Cache::forget('home_calendar');
        }

        return redirect()->route('events.index', ['drafted' => '1']);
    }

    public function uploadImage(UploadImage $request)
    {
        $path = $request->file('image')->store('public/events/tmp_'.$request->session()->get('tmp_asset_dir'));

        return new JsonResponse(Storage::url($path));
    }

    private function isOnHomepage(Event $event): bool
    {
        if (true === $event->draft) {
            return false;
        }

        return strtotime($event->date) < strtotime('+6 months', time());
    }
}
