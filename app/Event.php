<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class Event extends Model
{
    use Sluggable;

    const RULES = [
            'title' => ['required', 'max:60', 'min:3'],
            'description' => ['required', 'max:13120', 'min:60'],
            'date' => ['required', 'date', 'after:yesterday', 'before:+6 months'],
            'time_start' => ['required', 'date_format:H:i'],
            'time_end' => ['nullable', 'date_format:H:i', 'after:time_start'],
            'location' => ['nullable', 'string', 'max:60', 'min:3'],
            'contact' => ['nullable', 'email'],
        ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'date',
        'time_start',
        'time_end',
        'location',
        'contact',
    ];

    protected static function booted()
    {
        static::deleted(fn ($event) => self::clean($event->slug));
    }

    public static function findSlug(string $slug, array $select = ['*']): self
    {
        return static::where('slug', $slug)->select($select)->firstOrFail();
    }

    public static function retrieveEvent(string $slug): self
    {
        $cacheKey = 'event_'.$slug;
        $event = Cache::get($cacheKey);

        if (null !== $event) {
            return self::filterExpired($event);
        }

        // Get the event from the database if not cached
        $event = self::findSlug($slug);

        // Cache it if published
        if (! $event->draft) {
            Cache::put($cacheKey, $event);
        }

        return self::filterExpired($event);
    }

    /**
     * Delete the event if expired and throw a 404 HttpException.
     */
    private static function filterExpired(self $event): self
    {
        if (today()->timestamp <= strtotime($event->date)) {
            return $event;
        }

        $event->delete();
        Log::debug('Event deleted on retrieval tentative.');
        abort(404);

        return $event;
    }

    public static function clean(string $slug)
    {
        if (Storage::exists('public/events/'.$slug)) {
            Storage::deleteDirectory('public/events/'.$slug);
            Log::critical('Directory deleted: public/events/'.$slug);
        }

        Cache::forget('event_'.$slug);
        Log::critical('Deleted: '.$slug);
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title',
            ],
        ];
    }
}
