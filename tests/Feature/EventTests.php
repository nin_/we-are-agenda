<?php

namespace Tests\Feature;

use App\Event;

trait EventTests
{
    private function factoryEventToFormData(Event $event): array
    {
        // Remove time (from $faker->dateTimeBetween)
        $event->date = null !== $event->date ? date_format($event->date, 'Y-m-d') : null;

        // Add `name` & `time` honeypots for spam bot detection
        return array_merge($event->toArray(), ['name' => '', 'time' => time() - 5]);
    }
}
