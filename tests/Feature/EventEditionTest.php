<?php

namespace Tests\Feature;

use Tests\TestCase;

class EventEditionTest extends TestCase
{
    use EventTests;

    public function testShowEditPageAsAdmin()
    {
        $this->actingAs(factory('App\User')->create());
        $event = factory('App\Event')->create(['draft' => true]);

        $response = $this->get('/events/'.$event->slug.'/edit');
        $response->assertSeeInOrder(['Edit the event', $event['name']]);
    }

    /**
     * @depends testShowEditPageAsAdmin
     */
    public function testEditDraftedEventAsAdmin()
    {
        $this->actingAs(factory('App\User')->create());
        $event = $this->factoryEventToFormData(factory('App\Event')->create(['draft' => true, 'title' => 'original']));

        $event['title'] = 'edited'.rand();

        $response = $this->post('/events/'.$event['slug'].'/edit', $event);

        $response->assertRedirect('/events/'.$event['slug']);
        $response = $this->followRedirects($response);

        $response->assertSeeInOrder(['Moderation', 'value="Publish"', $event['title']], false);
    }

    /**
     * @depends testShowEditPageAsAdmin
     */
    public function testEditPublishedEventAsAdmin()
    {
        $this->actingAs(factory('App\User')->create());
        $event = $this->factoryEventToFormData(factory('App\Event')->create(['draft' => false, 'title' => 'original']));

        $event['title'] = 'edited'.rand();

        $response = $this->post('/events/'.$event['slug'].'/edit', $event);
        $response->assertRedirect('/events/'.$event['slug']);

        $response = $this->followRedirects($response);

        $response->assertSeeInOrder(['Moderation', $event['title']], false);
        $response->assertDontSee('value="Publish"', false);
    }

    public function testTryToEditEventAsAnon()
    {
        $event = $this->factoryEventToFormData(factory('App\Event')->create(['title' => 'original']));

        $event['title'] = 'edited'.rand();

        $response = $this->post('/events/'.$event['slug'].'/edit', $event);
        $response->assertRedirect('/login');

        $response = $this->get('/events/'.$event['slug']);
        $response->assertDontSee($event['title']);
    }

    /**
     * @depends testEditPublishedEventAsAdmin
     */
    public function testCheckForUpdatedEventOnMainPage()
    {
        $this->actingAs(factory('App\User')->create());
        $event = $this->factoryEventToFormData(factory('App\Event')->create([
            'draft' => false,
            'date' => today(),
        ]));

        $oldTitle = $event['title'];
        $event['title'] = 'edited'.rand();
        $this->post('/events/'.$event['slug'].'/edit', $event);

        $response = $this->get('/');
        $response->assertDontSee($oldTitle);
        $response->assertSee($event['title']);
    }
}
