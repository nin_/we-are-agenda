<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class AdminConsoleTest extends TestCase
{
    use RefreshDatabase;

    public function testAdminCreation()
    {
        $this->artisan('admin:create tester')
            ->expectsQuestion('Password?', "Please don't look")
            ->assertExitCode(0);

        $this->assertCredentials(['name' => 'tester', 'password' => "Please don't look"]);
    }

    /**
     * @depends testAdminCreation
     */
    public function testAdminDeletion()
    {
        $this->artisan('admin:create tester')
            ->expectsQuestion('Password?', "Please don't look");

        $this->artisan('admin:delete tester');

        $this->assertSame(0, User::where('name', 'tester')->count());
    }

    public function testEditName()
    {
        $user = factory('App\User')->create([
            'name' => 'originalName',
            'password' => Hash::make('foobarbaz'),
        ]);

        $this->artisan('admin:edit originalName')
            ->expectsConfirmation('Change name?', 'yes')
            ->expectsQuestion('New name:', 'editedName')
            ->expectsConfirmation('Change password?')
            ->expectsConfirmation('Delete email?')
            ->expectsConfirmation('Edit email?');

        $this->assertCredentials(['name' => 'editedName', 'password' => 'foobarbaz']);
    }

    public function testEditPassword()
    {
        $user = factory('App\User')->create();

        $this->artisan('admin:edit '.$user->name)
            ->expectsConfirmation('Change name?')
            ->expectsConfirmation('Change password?', 'yes')
            ->expectsQuestion('New password:', 'editedPassword')
            ->expectsConfirmation('Delete email?')
            ->expectsConfirmation('Edit email?');

        $this->assertCredentials(['name' => $user->name, 'password' => 'editedPassword']);
    }

    public function testEditEmail()
    {
        $user = factory('App\User')->create(['email' => 'email@original.test']);

        $this->artisan('admin:edit '.$user->name)
            ->expectsConfirmation('Change name?')
            ->expectsConfirmation('Change password?')
            ->expectsConfirmation('Delete email?')
            ->expectsConfirmation('Edit email?', 'yes')
            ->expectsQuestion('New email:', 'email@edited.test');

        $this->assertSame('email@edited.test', User::firstWhere('name', $user->name)->email);
    }

    public function testAdminEditAllInformations()
    {
        factory('App\User')->create([
            'email' => 'email@original.test',
            'name' => 'originalName',
            'password' => Hash::make('originalPassword'),
        ]);

        // Edit the name
        $this->artisan('admin:edit originalName')
            ->expectsConfirmation('Change name?', 'yes')
            ->expectsQuestion('New name:', 'editedName')
            ->expectsConfirmation('Change password?', 'yes')
            ->expectsQuestion('New password:', 'editedPassword')
            ->expectsConfirmation('Delete email?')
            ->expectsConfirmation('Edit email?', 'yes')
            ->expectsQuestion('New email:', 'email@edited.test');

        $this->assertCredentials(['name' => 'editedName', 'password' => 'editedPassword']);
        $this->assertSame('email@edited.test', User::firstWhere('name', 'editedName')->email);
    }

    public function testAdminAddEmail()
    {
        $user = factory('App\User')->create([
            'email' => null,
        ]);

        // Edit the name
        $this->artisan('admin:edit '.$user->name)
            ->expectsConfirmation('Change name?')
            ->expectsConfirmation('Change password?')
            ->expectsConfirmation('Add email?', 'yes')
            ->expectsQuestion('Email:', 'foo@bar.baz');

        $this->assertSame('foo@bar.baz', User::firstWhere('name', $user->name)->email);
    }

    public function testAdminDeleteEmail()
    {
        $user = factory('App\User')->create([
            'email' => 'foo@bar.baz',
        ]);

        // Edit the name
        $this->artisan('admin:edit '.$user->name)
            ->expectsConfirmation('Change name?')
            ->expectsConfirmation('Change password?')
            ->expectsConfirmation('Delete email?', 'yes');

        $this->assertSame(null, User::firstWhere('name', $user->name)->email);
    }
}
