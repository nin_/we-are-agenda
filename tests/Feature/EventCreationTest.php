<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Tests\TestCase;

class EventCreationTest extends TestCase
{
    use EventTests;

    /**
     * I can submit an event with:
     * - a title
     * - a description
     * - a date
     * - a start time
     * - an end time
     * - a category (not ready)
     * - a location
     * - an email.
     */
    public function testSubmitCompleteEvent()
    {
        $event = $this->factoryEventToFormData(factory('App\Event')->make([
            // Add optional values
            'contact' => 'lorem@impsum.test',
            'location' => 'Somewhere nice',
            'time_end' => '23:59',
        ]));

        $response = $this->followingRedirects()->json('post', '/events', $event);

        $response->assertOk();
        $response->assertSee('This event awaits moderation');

        $response = $this->actingAs(factory('App\User')->create())
            ->get('/events/'.$event['slug']);

        // Check all values
        $response->assertSeeInOrder([
            $event['name'],
            $event['contact'],
            Carbon::parse($event['date'])->isoFormat('LL'),
            $event['time_start'],
            $event['time_end'],
            $event['location'],
        ]);
    }

    /**
     * I can submit an event without:
     * - an end time
     * - a category
     * - a location
     * - an email.
     */
    public function testSubmitIncompleteButValidEvent()
    {
        $event = $this->factoryEventToFormData(factory('App\Event')->make([
            'location' => null,
            'contact' => null,
        ]));

        $response = $this->followingRedirects()->json('post', '/events', $event);

        $response->assertOk();
        $response->assertSeeInOrder([$event['name'], 'This event awaits moderation']);
    }

    /**
     * I can't submit an event without:
     * - a title
     * - a description
     * - a date
     * - a start time.
     */
    public function testSubmitMissingRequiredInfo()
    {
        foreach (['title', 'description', 'date', 'time_start'] as $param) {
            $event = $this->factoryEventToFormData(factory('App\Event')->make([$param => null]));
            $response = $this->followingRedirects()->json('post', '/events', $event);
            $response->assertStatus(422);
            $response->assertSee('The given data was invalid.');
        }
    }
}
