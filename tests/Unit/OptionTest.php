<?php

namespace Tests\Unit;

use App\Http\Controllers\OptionController;
use Error;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use PHPUnit\Framework\TestCase;

class OptionTest extends TestCase
{
    protected function setUp(): void
    {
        // Laravel facades mockery
        Cache::shouldReceive()->with()->getMock()->shouldIgnoreMissing();
        DB::shouldReceive('selectOne')->andReturnNull();
    }

    public function testGetInvalidOption()
    {
        $this->expectException(Error::class);

        try {
            OptionController::getOption('not_an_option');
        } catch (Error $e) {
            // The method `abort` don't exist in the context of a unit test but this is the expected behavior.
            $this->assertSame('Call to undefined method Illuminate\Container\Container::abort()', $e->getMessage());
            throw $e;
        }
    }

    public function testGetOption()
    {
        $option = OptionController::getOption('locked');
        $this->assertSame(false, $option);
    }

    public function testGetInvalidBanner()
    {
        $this->expectException(Error::class);

        try {
            OptionController::getBanner('not_a_banner');
        } catch (Error $e) {
            // The method `abort` don't exist in the context of a unit test but this is the expected behavior.
            $this->assertSame('Call to undefined method Illuminate\Container\Container::abort()', $e->getMessage());
            throw $e;
        }
    }

    public function testGetBanner()
    {
        $banner = OptionController::getBanner('home');

        $this->assertIsString($banner);
    }
}
