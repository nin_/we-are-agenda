<?php

namespace Tests\Browser;

use App\Event;
use App\User;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Admin\Edit;
use Tests\Browser\Pages\Events\Create;
use Tests\Browser\Pages\Events\Show;
use Tests\DuskTestCase;

class EventsTest extends DuskTestCase
{
    /**
     * This test the `no_review` option and it also ensure that `no_review` is set to false for following tests.
     */
    public function testSetReview()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            $browser->loginAs($user)
                ->visit(new Edit('no_review'))
                ->submitOption(true);

            $browser->logout();
            $browser->visit('/')
                ->clickLink('Publish an event')
                ->waitForRoute('events.create')
                ->assertMissing('div[role=alert]');

            $browser->loginAs($user)
                ->visit(new Edit('no_review'))
                ->submitOption(false);

            $browser->logout();
            $browser->visit('/')
                ->clickLink('Publish an event')
                ->waitForRoute('events.create')
                ->assertPresent('div[role=alert]');
        });
    }

    public function testSubmitValidEvent()
    {
        $this->browse(function (Browser $browser) {
            $event = factory(Event::class)->make(['description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit viverra fusce.']);

            $this->submit($browser)->submitValidEvent($event)
                ->assertRouteIs('events.show', ['event' => $event->slug])
                ->on(new Show($event))
                ->assertEventWaitingForValidation();
        });
    }

    public function testSubmitPastDate()
    {
        $this->browse(function (Browser $browser) {
            $event = factory(Event::class)->make([
                'date' => date_create('1990-01-01'),
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit viverra fusce.',
            ]);

            $this->submit($browser)->submitValidEvent($event)
                ->assertRouteIs('events.create');
        });
    }

    public function testSubmitTooShortDescription()
    {
        $this->browse(function (Browser $browser) {
            $event = factory(Event::class)->make(['description' => 'short desc']);

            $this->submit($browser)->submitValidEvent($event)
                ->assertRouteIs('events.create')
                ->assertSee('The description must be at least 60 characters');
        });
    }

    public function testSubmitStartAfterEnd()
    {
        $this->browse(function (Browser $browser) {
            $event = factory(Event::class)->make([
                'time_start' => '13:12',
                'time_end' => '12:12',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit viverra fusce.',
            ]);

            $this->submit($browser)->submitValidEvent($event)
                ->assertRouteIs('events.create')
                ->assertSee('The time end must be a date after time start');
        });
    }

    public function testSubmitInvalidEmail()
    {
        $this->browse(function (Browser $browser) {
            $event = factory(Event::class)->make([
                'contact' => 'notAnEmail',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit viverra fusce.',
            ]);

            $this->submit($browser)->submitValidEvent($event)
                ->assertRouteIs('events.create');
        });
    }

    private function submit(Browser $browser): Browser
    {
        return $browser->visit('/')
            ->clickLink('Publish an event')
            ->waitForRoute('events.create')
            ->on(new Create);
    }

    public function testApprove()
    {
        $user = factory('App\User')->create();
        $event = factory('App\Event')->create(['draft' => true]);

        $this->browse(function (Browser $browser) use ($user, $event) {
            $browser->loginAs($user)
                ->visitRoute('events.show', ['event' => $event->slug])
                ->click('input[value=Publish]')
                ->assertRouteIs('events.index')
                ->assertQueryStringHas('drafted', 1);

            $browser->visitRoute('events.show', ['event' => $event->slug])
                ->on(new Show($event))
                ->assertEventPublished();
        });
    }

    public function testReject()
    {
        $user = factory('App\User')->create();
        $event = factory('App\Event')->create(['draft' => true]);

        $this->browse(function (Browser $browser) use ($user, $event) {
            $browser->loginAs($user)
                ->visitRoute('events.show', ['event' => $event->slug])
                ->click('input[value=Delete]')
                ->assertRouteIs('events.index')
                ->assertQueryStringHas('drafted', 1);

            $browser->visitRoute('events.show', ['event' => $event->slug])
                ->assertSee('Not Found');
        });
    }

    /**
     * @depends testSetReview
     */
    public function testSubmitValidEventWithoutValidation()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            $browser->loginAs($user)
                ->visit(new Edit('no_review'))
                ->submitOption(true);

            $event = factory(Event::class)->make(['description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit viverra fusce.']);

            $this->submit($browser)->submitValidEvent($event)
                ->assertRouteIs('events.show', ['event' => $event->slug])
                ->on(new Show($event))
                ->assertEventPublished();
        });
    }
}
