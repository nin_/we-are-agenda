<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Events\Edit;
use Tests\Browser\Pages\Events\Show;
use Tests\DuskTestCase;

class EventsEditTest extends DuskTestCase
{
    public function testEditPublishedAsAdmin()
    {
        $user = factory('App\User')->create();
        $event = factory('App\Event')->create(['draft' => false, 'title' => 'original']);

        $newTitle = 'updated'.rand();

        $this->browse(function (Browser $browser) use ($user, $event, $newTitle) {
            $browser->loginAs($user)
                ->visit(new Show($event))
                ->click('a.btn.blue')
                ->assertRouteIs('events.edit', ['event' => $event->slug])
                ->on(new Edit($event->slug))
                ->update(['title' => $newTitle])
                ->on(new Show($event))
                ->assertSee($newTitle)
                ->assertEventPublished();
        });
    }

    public function testEditDraftedAsAdmin()
    {
        $user = factory('App\User')->create();
        $event = factory('App\Event')->create(['draft' => true, 'title' => 'original']);

        $newTitle = 'updated'.rand();

        $this->browse(function (Browser $browser) use ($user, $event, $newTitle) {
            $browser->loginAs($user)
                ->visit(new Show($event))
                ->click('a.btn.blue')
                ->assertRouteIs('events.edit', ['event' => $event->slug])
                ->on(new Edit($event->slug))
                ->update(['title' => $newTitle])
                ->on(new Show($event))
                ->assertEventWaitingForValidation($newTitle);
        });
    }

    public function testEditWithShortDescriptionAsAdmin()
    {
        $user = factory('App\User')->create();
        $event = factory('App\Event')->create();

        $this->browse(function (Browser $browser) use ($user, $event) {
            $browser->loginAs($user)
                ->visit(new Edit($event->slug))
                ->update(['description' => 'short desc'], false)
                ->assertSee('The description must be at least 60 characters')
                ->assertRouteIs('events.edit', ['event' => $event->slug]);
        });
    }
}
