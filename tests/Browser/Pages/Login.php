<?php

namespace Tests\Browser\Pages;

use App\User;
use Laravel\Dusk\Browser;

class Login extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/login';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param Browser $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
    }

    public function submitIncorrectForm(Browser $browser)
    {
        $browser->type('name', 'admin')
            ->type('password', 'password')
            ->click('@submit');
    }

    public function submitValidForm(Browser $browser, User $user)
    {
        $browser->type('name', $user->name)
            ->type('password', $user->password)
            ->click('@submit');
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@element' => '#selector',
        ];
    }
}
