<?php

namespace Tests\Browser\Pages\Admin;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class Edit extends Page
{
    protected string $option;

    public function __construct(string $option)
    {
        $this->option = $option;
    }

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/admin/'.$this->option.'/edit';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathBeginsWith($this->url());
    }

    public function submitOption(Browser $browser, $value)
    {
        if (gettype($value) === 'boolean') {
            $value ? $browser->check('value') : $browser->uncheck('value');
            $browser->click('input[value=Update]');
            return;
        }

        $browser->type('value', $value)->click('input[value=Update]');
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@element' => '#selector',
        ];
    }
}
