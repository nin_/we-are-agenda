<?php

namespace Tests\Browser\Pages\Events;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class Edit extends Page
{
    protected string $slug;

    public function __construct(string $slug)
    {
        $this->slug = $slug;
    }

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/events/'.$this->slug.'/edit';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param Browser $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
    }

    public function update(Browser $browser, array $data, bool $waitForSuccess = true)
    {
        foreach ($data as $field => $value) {
            $browser->type($field, $value);
        }

        $browser->click('input[type=submit].btn');

        if (! $waitForSuccess) {
            return;
        }

        $browser->waitForRoute('events.show', $this->slug);
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@element' => '#selector',
        ];
    }
}
