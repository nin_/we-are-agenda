<?php

namespace Tests\Browser\Pages\Events;

use App\Event;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class Show extends Page
{
    protected Event $event;

    public function __construct(Event $event)
    {
        $this->event = $event;
    }

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/events/'.$this->event->slug;
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathBeginsWith($this->url());
    }

    public function assertEventWaitingForValidation(Browser $browser, ?string $expectedTitle = null)
    {
        $browser->assertPresent('@alert')
            ->within('@alert', function (Browser $browser) {
                $browser->assertSee('This event awaits moderation');
            });

        if (null === $expectedTitle) {
            $browser->assertSee($this->event->title);

            return;
        }

        $browser->assertSee($expectedTitle);
    }

    public function assertEventPublished(Browser $browser)
    {
        $browser->assertDontSee('This event awaits moderation');
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@alert' => 'div[role=alert].bg-teal-100',
        ];
    }
}
