<?php

namespace Tests\Browser\Pages\Events;

use App\Event;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class Create extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/events/create';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param Browser $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
    }

    public function submitValidEvent(Browser $browser, Event $event)
    {
        $browser->type('title', $event->title)
            ->type('date', $event->date->format('Y-m-d'))
            ->type('time_start', $event->time_start)
            ->type('time_end', $event->time_end)
            ->type('location', $event->location)
            ->type('description', $event->description)
            ->type('contact', $event->contact)
            // bypass spam bot detection
            ->value('input[name=time]', time() - 5)
            ->click('input[type=submit].btn');
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@element' => '#selector',
        ];
    }
}
