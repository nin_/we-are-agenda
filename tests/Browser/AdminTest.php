<?php

namespace Tests\Browser;

use App\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Admin\Edit;
use Tests\Browser\Pages\Admin\Index;
use Tests\Browser\Pages\Login;
use Tests\DuskTestCase;

class AdminTest extends DuskTestCase
{
    use WithFaker;

    public function testAdminLoginFail()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit(new Login)
                ->submitIncorrectForm()
                ->assertRouteIs('login')
                ->assertSee('These credentials do not match our records.');
        });
    }

    public function testAdminLoginSucceed()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create(['password' => Hash::make('foobarbaz')]);
            $user->password = 'foobarbaz';

            $browser->visit(new Login)
                ->submitValidForm($user)
                ->assertRouteIs('events.index');
        });
    }

    public function testAdminEditHomeBanner()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();
            $value = $this->faker->text;

            $browser
                ->loginAs($user)
                ->visit(new Index())
                ->editOption('home_banner')
                ->waitForRoute('admin.edit', ['admin' => 'home_banner'])
                ->on(new Edit('home_banner'))
                ->waitForText('home_banner')
                ->submitOption($value)
                ->assertValue("#value", $value);

            $browser->visit('/')->assertSee($value);
        });
    }
}
