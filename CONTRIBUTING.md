# Contributing to We Are Agenda
Thank you for considering to contribute to We Are Agenda.

## How to contribute
You can help in many ways, examples of contributions you can make:

- Report bugs, make suggestions, ask for features.
- Make design propositions, make illustrations, help on the user experience and interface.
- Add a translation or improve an existing one.
- Improve documentation.
- Help on development.


## Code Of Conduct
You must have read and respect our [CODE_OF_CONDUCT.md](CODE_OF_CONDUCT.md).

## Contribute
### Report a bug
Open a ticket on the [issue tracker](https://gitlab.com/nin_/we-are-agenda/issues) and describe the bug.

You should include :
- The expected behaviour
- Steps to reproduce

### Submit a suggestion or ask for a feature
Open a ticket on the [issue tracker](https://gitlab.com/nin_/we-are-agenda/issues) and describe your idea.

### Contribute to code
#### Copyrights
When contributing to this project you agree that your changes will be licensed under the GNU Affero General Public License v3.0.

You also waive any claim to copyright.

#### Commit message
Please format your commits with the following pattern :

```
<type>(#<issue>): <description>
```

**types** :

- `add`: non-breaking change which adds functionality
- `ui`: interface change
- `fix`: non-breaking change which fixes an issue
- `doc`: change and addition to the documentation
- `tech`: potentially breaking change without functional changes which refine or optimize existing code 
- `style`: code style related (non-breaking change, no functional changes)
- `test`: addition and edition of tests
- `chore`: changes of build/server scripts

#### Merge request process
1. Ensure any install or build dependencies aren't in your merge request.
2. Update the README.md if it's outdated by your changes.
4. You may merge the merge request in once you have the sign-off of one other developers.
5. If you do not have permission to do that, you may request the second reviewer to merge it for you.
