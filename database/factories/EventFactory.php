<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Event;
use Faker\Generator as Faker;
use DavidBadura\FakerMarkdownGenerator\FakerProvider as FakerMarkdown;
use \Cviebrock\EloquentSluggable\Services\SlugService;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Event::class, function (Faker $faker) {
    $faker->addProvider(new FakerMarkdown($faker));

    $title = $faker->sentence(4);
    return [
        'title' => $title,
        'slug' =>  SlugService::createSlug(Event::class, 'slug', $title),
        'date' => $faker->dateTimeBetween('now', '+6 months'),
        'description' => $faker->markdown(),
        'time_start' => $faker->time('H:i', '23:58'),
        'location' => $faker->optional()->address,
        'draft' => $faker->boolean(10),
        'contact' => $faker->optional()->email,
    ];
});
